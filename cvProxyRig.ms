/*
Copyright (C) 2019 Victor Chavanne <victor.chavanne@tutanota.com>

This file is part of cvAutoRig.

cvAutoRig is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

cvAutoRig is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with cvAutoRig.  If not, see <https://www.gnu.org/licenses/>.
*/

-- ### COLORS ###
udWhite = color 255 255 255
udPink = color 255 100 200
udPurpleDark = color 150 0 150
udPurpleLight = color 250 0 250
udCyanDark = color 0 150 150
udCyanLight = color 0 250 250
udGreenDark = color 0 150 0
udGreenLight = color 0 250 0
udOrangeDark = color 200 25 0
udOrangeLight = color 250 75 0
udYellowDark = color 150 150 0
udYellowLight = color 250 250 100
udBlueDark = color 0 0 150
udBlueLight = color 0 0 250

-- ### FUNCTIONS ###
function createCVnode = (
	if (getNodeByName "CVnode") == undefined do (
		CVnode = point wirecolor:udWhite drawontop:false box:true cross:false axistripod:false centermarker:false size:50 name:"CVnode"
		setUserProp $CVnode "Body parts" ""
	)
)

function newBodyParts nbName nbColor:udPink nbSize:10 nbParent:undefined nbMove:[0, 0, 0] nbRotate:(eulerangles 0 0 0) nbBox:false nbCross:false nbAxis:true nbIsParent:false = (
	newNode = point wirecolor:nbColor size:nbSize drawontop:true box:nbBox cross:nbCross axistripod:nbAxis centermarker:false name:nbName
	if nbIsParent == true and nbParent != undefined then (
		newNode.pos = nbParent.pos
		newNode.parent = nbParent
	)
	else if nbIsParent == false and nbParent != undefined then (
		newNode.transform = nbParent.transform
	)
	in coordsys local move newNode nbMove
	rotate newNode nbRotate
	newNode.parent = nbParent
	setUserProp $CVnode "Body parts" ((getUserProp $CVnode "Body parts") + " " + nbName)
	newNode
)

function makeRightArm attachNode:undefined nbOffset:[0, 0, 0] = (
	-- Create parent node
	RAMainCtrl = newBodyParts "RightArm_Parent" nbColor:udPurpleDark nbSize:40 nbParent:attachNode nbMove:nbOffset nbRotate:(eulerangles 0 0 180) nbBox:true nbCross:false nbAxis:false nbIsParent:true
	-- Create body parts
	RACalicle = newBodyParts "RightArm_Clavicle" nbColor:udPurpleLight nbSize:10 nbParent:RAMainCtrl
	RAShoulderCtrl = newBodyParts "RightArm_ShoulderPivot" nbColor:udPurpleLight nbSize:10 nbParent:RAMainCtrl nbMove:[10, 0, 0]
	RAElbowCtrl = newBodyParts "RightArm_ElbowPivot" nbColor:udPurpleLight nbSize:10 nbParent:RAMainCtrl nbMove:[30, -5, 0]
	RAWristCtrl = newBodyParts "RightArm_WristPivot" nbColor:udPurpleLight nbSize:10 nbParent:RAMainCtrl nbMove:[50, 0, 0]
)

function makeRightHand attachNode:undefined nbOffset:[0, 0, 0] = (
	-- Create parent node
	RHMainCtrl = newBodyParts "RightHand_Parent" nbColor:udCyanDark nbSize:30 nbParent:attachNode nbMove:nbOffset nbRotate:(eulerangles 0 0 180) nbBox:true nbCross:false nbAxis:false nbIsParent:true
	-- Create auriculaire
	RHAuri01 = newBodyParts "RightHand_Auri01Pivot" nbColor:udCyanLight nbSize:5 nbParent:RHMainCtrl nbMove:[5, 6, 0]
	RHAuri02 = newBodyParts "RightHand_Auri02Pivot" nbColor:udCyanLight nbSize:5 nbParent:RHAuri01 nbMove:[8, 0, 0]
	RHAuri03 = newBodyParts "RightHand_Auri03Pivot" nbColor:udCyanLight nbSize:5 nbParent:RHAuri02 nbMove:[4, 0, 0]
	RHAuri04 = newBodyParts "RightHand_Auri04Pivot" nbColor:udCyanLight nbSize:5 nbParent:RHAuri03 nbMove:[4, 0, 0]
	-- Create Anulaire
	RHAnu01 = newBodyParts "RightHand_Anu01Pivot" nbColor:udCyanLight nbSize:5 nbParent:RHMainCtrl nbMove:[5, 2, 0]
	RHAnu02 = newBodyParts "RightHand_Anu02Pivot" nbColor:udCyanLight nbSize:5 nbParent:RHAnu01 nbMove:[8, 0, 0]
	RHAnu03 = newBodyParts "RightHand_Anu03Pivot" nbColor:udCyanLight nbSize:5 nbParent:RHAnu02 nbMove:[4, 0, 0]
	RHAnu04 = newBodyParts "RightHand_Anu04Pivot" nbColor:udCyanLight nbSize:5 nbParent:RHAnu03 nbMove:[4, 0, 0]
	-- Create Majeur
	RHMaj01 = newBodyParts "RightHand_Maj01Pivot" nbColor:udCyanLight nbSize:5 nbParent:RHMainCtrl nbMove:[5, -2, 0]
	RHMaj02 = newBodyParts "RightHand_Maj02Pivot" nbColor:udCyanLight nbSize:5 nbParent:RHMaj01 nbMove:[8, 0, 0]
	RHMaj03 = newBodyParts "RightHand_Maj03Pivot" nbColor:udCyanLight nbSize:5 nbParent:RHMaj02 nbMove:[4, 0, 0]
	RHMaj04 = newBodyParts "RightHand_Maj04Pivot" nbColor:udCyanLight nbSize:5 nbParent:RHMaj03 nbMove:[4, 0, 0]
	-- Create Index
	RHIndex01 = newBodyParts "RightHand_Index01Pivot" nbColor:udCyanLight nbSize:5 nbParent:RHMainCtrl nbMove:[5, -6, 0]
	RHIndex02 = newBodyParts "RightHand_Index02Pivot" nbColor:udCyanLight nbSize:5 nbParent:RHIndex01 nbMove:[8, 0, 0]
	RHIndex03 = newBodyParts "RightHand_Index03Pivot" nbColor:udCyanLight nbSize:5 nbParent:RHIndex02 nbMove:[4, 0, 0]
	RHIndex04 = newBodyParts "RightHand_Index04Pivot" nbColor:udCyanLight nbSize:5 nbParent:RHIndex03 nbMove:[4, 0, 0]
	-- Create Thumb
	RHThumb01 = newBodyParts "RightHand_Thumb01Pivot" nbColor:udCyanLight nbSize:5 nbParent:RHMainCtrl nbMove:[0, 8, 0] nbRotate:(eulerangles 25 -20 45)
	RHThumb02 = newBodyParts "RightHand_Thumb02Pivot" nbColor:udCyanLight nbSize:5 nbParent:RHThumb01 nbMove:[8, 0, 0] nbRotate:(eulerangles 0 0 -15)
	RHThumb03 = newBodyParts "RightHand_Thumb03Pivot" nbColor:udCyanLight nbSize:5 nbParent:RHThumb02 nbMove:[4, 0, 0] nbRotate:(eulerangles 0 0 -15)
)

function makeRightLeg attachNode:undefined nbOffset:[0, 0, 0] = (
	-- Create parent node
	RLMainCtrl = newBodyParts "RightLeg_Parent" nbColor:udGreenDark nbSize:30 nbParent:attachNode nbMove:nbOffset nbRotate:(eulerangles 0 90 180) nbBox:true nbCross:false nbAxis:false nbIsParent:true
	-- Create body parts
	RLHipCtrl = newBodyParts "RightLeg_Hip" nbColor:udGreenLight nbSize:10 nbParent:RLMainCtrl
	RLKneeCtrl = newBodyParts "RightLeg_KneePivot" nbColor:udGreenLight nbSize:10 nbParent:RLMainCtrl nbMove:[40, 10, 0]
	RLAnkle = newBodyParts "RightLeg_AnklePivot" nbColor:udGreenLight nbSize:10 nbParent:RLMainCtrl nbMove:[80, 0, 0]
)

function makeRightFeet attachNode:undefined nbOffset:[0, 0, 0] = (
	-- Create parent node
	RFMainCtrl = newBodyParts "RightFeet_Parent" nbColor:udOrangeDark nbSize:30 nbParent:attachNode nbMove:nbOffset nbRotate:(eulerangles 90 0 -90) nbBox:true nbCross:false nbAxis:false nbIsParent:true
	-- Create body parts
	RFAnkleCtrl = newBodyParts "RightFeet_AnklePivot" nbColor:udOrangeLight nbSize:10 nbParent:RFMainCtrl
	RFToeCtrl = newBodyParts "RightFeet_ToesPivot" nbColor:udOrangeLight nbSize:5 nbParent:RFMainCtrl nbMove:[10, -3, 0]
	RFToeNubCtrl = newBodyParts "RightFeet_ToesNubsPivot" nbColor:udOrangeLight nbSize:5 nbParent:RFMainCtrl nbMove:[15, -3, 0]
)

function makeSpine attachNode:undefined nbOffset:[0, 0, 0] = (
	-- Create parent node
	PMainCtrl = newBodyParts "Pelvis_Parent" nbColor:udBlueDark nbSize:30 nbParent:attachNode nbMove:nbOffset nbRotate:(eulerangles 180 -90 0) nbBox:true nbCross:false nbAxis:false nbIsParent:true
	PPevlisCtrl = newBodyParts "Pelivs_Pivot" nbColor:udBlueDark nbSize:10 nbParent:PMainCtrl
	-- Create body parts
	SSpine_01 = newBodyParts "Spine_01Pivot" nbColor:udBlueLight nbSize:10 nbParent:PMainCtrl nbMove:[20, 0, 0]
	SSpine_02 = newBodyParts "Spine_02Pivot" nbColor:udBlueLight nbSize:10 nbParent:PMainCtrl nbMove:[40, 0, 0]
	SSpine_03 = newBodyParts "Spine_03Pivot" nbColor:udBlueLight nbSize:10 nbParent:PMainCtrl nbMove:[60, 0, 0]
	SSpine_04 = newBodyParts "Spine_04Pivot" nbColor:udBlueLight nbSize:10 nbParent:PMainCtrl nbMove:[80, 0, 0]
	SSpine_05 = newBodyParts "Spine_05NubPivot" nbColor:udBlueLight nbSize:10 nbParent:PMainCtrl nbMove:[100, 0, 0]
)

function makeNeckAndHead attachNode:undefined nbOffset:[0, 0, 0] = (
	-- Create parent node
	NMainCtrl = newBodyParts "Neck_Parent" nbColor:udYellowDark nbSize:30 nbParent:attachNode nbMove:nbOffset nbRotate:(eulerangles 180 -90 0) nbBox:true nbCross:false nbAxis:false nbIsParent:true
	-- Create body parts
	NNeck_01 = newBodyParts "Neck_Pivot" nbColor:udYellowLight nbNize:5 nbParent:NMainCtrl
	NHead_01 = newBodyParts "Neck_HeadPivot" nbColor:udYellowDark nbSize:10 nbParent:NMainCtrl nbMove:[10, 0, 0]
	NHead_02 = newBodyParts "Neck_HeadNubPivot" nbColor:udYellowDark nbSize:10 nbParent:NMainCtrl nbMove:[30, 0, 0]
)

try(destroyDialog autoRigRollout )catch()
rollout autoRigRollout "autoRig" (
	-- Add CVnode
	on autoRigRollout open do createCVnode()
	subRollout proxyRigSub height:250
)
rollout proxyRigRollout "Proxy Rig" (
	-- Add construction objects
	button addRightArmBtn "add right arm"
	on addRightArmBtn pressed do (makeRightArm attachNode:selection[1])
	button addRightHandBtn "add right hand"
	on addRightHandBtn pressed do (makeRightHand attachNode:selection[1])
	button addRightLegBtn "add right leg"
	on addRightLegBtn pressed do (makeRightLeg attachNode:selection[1])
	button addRightFeetBtn "add right feet"
	on addRightFeetBtn pressed do (makeRightFeet attachNode:selection[1])
	button addSpineBtn "add spine"
	on addSpineBtn pressed do (makeSpine attachNode:selection[1])
	button addNeckBtn "add neck and head"
	on addNeckBtn pressed do (makeNeckAndHead attachNode:selection[1])
	button addBipedBtn "Add biped"
	on addBipedBtn pressed do (
		makeSpine()
		makeRightLeg attachNode:$Pelvis_Parent nbOffset:[-20, 0, 0]
		makeRightFeet attachNode:$RightLeg_AnklePivot
		makeRightArm attachNode:$Spine_05NubPivot
		makeRightHand attachNode:$RightArm_WristPivot
		makeNeckAndHead attachNode:$Spine_05NubPivot
	)
	-- Delete construction objects
	button deleteConstructionObjectsBtn "delete construction objects"
	on deleteConstructionObjectsBtn pressed do (
		nodeList = filterstring (getUserProp $CVnode "Body parts") " "
		for nodeName in nodeList do try(delete (getNodeByName nodeName all:true))catch()
		setUserProp $CVnode "Body parts" ""
	)
)
createDialog autoRigRollout 200 300
AddSubRollout autoRigRollout.proxyRigSub proxyRigRollout