/*
Copyright (C) 2019 Victor Chavanne <victor.chavanne@tutanota.com>

This file is part of cvAutoRig.

cvAutoRig is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

cvAutoRig is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with cvAutoRig.  If not, see <https://www.gnu.org/licenses/>.
*/

-- TODO 	make sure the IK chain is properly made
-- 			make sure all the nodes are here: shoulder, elbow, wrist and swivel

IKFKSwitcherCA= attributes IKFKSwitcherCA
(	
	parameters IKParameter rollout:IKFKRollout
	(
		IKFKCtrl type:#float ui:IKFKslider
		shoulderFKNode type:#node
		elbowFKNode type:#node
		wristFKNode type:#node
		shoulderIKNode type:#node
		elbowIKNode type:#node
		wristIKNode type:#node
		IKHandle type:#node
		swivelIKNode type:#node
	)
	rollout IKFKRollout "IK/FK Switch"
	(
		button switchToFKBtn "FK" across:2
		button switchToIKBtn "IK"
		slider IKFKslider range:[0, 1, 1] type:#integer visible:true
		on switchToFKBtn pressed do (
			IKFKslider.value = 0
			shoulderFKNode.transform = shoulderIKNode.transform
			elbowFKNode.transform = elbowIKNode.transform
			wristFKNode.transform = wristIKNode.transform
		)
		on switchToIKBtn pressed do (
			IKHandle.position = wristFKNode.position
			tempDist = distance elbowIKNode swivelIKNode
			tempPos = elbowFKNode.position
			mTemp = transmatrix ((shoulderFKNode.transform[4] + wristFKNode.transform[4])/2)
			mTemp[1] = normalize(wristFKNode.transform[4] - shoulderFKNode.transform[4])
			mTemp[2] = normalize(elbowFKNode.transform[4] - mTemp[4])
			mTemp[3] = cross mTemp[1] mTemp[2]
			mTemp[1] = cross mTemp[2] mTemp[3]
			swivelIKNode.transform = mTemp
			swivelIKNode.position = tempPos
			in coordsys local move swivelIKNode [0, tempDist, 0]
			shoulderFKNode.rotation.controller[#Animation].value = quat 0 0 0 1
			elbowFKNode.rotation.controller[#Animation].value = quat 0 0 0 1
			wristFKNode.rotation.controller[#Animation].value = quat 0 0 0 1
			IKFKslider.value = 100
		)
	)
)

function makeIKFK IKHandle elbowNode = (
	-- Init nodes
	shoulderIKNode = IKHandle.transform.controller.startJoint 
	elbowIKNode = elbowNode
	wristIKNode = IKHandle.transform.controller.endJoint 
	swivelIKNode = IKHandle.transform.controller.VHTarget
	-- Create FK Nodes aligned on IK nodes
	global shoulderFKNode = point size:5 cross:false box:true centermarker:false axistripod:false wirecolor:blue name:"help_fk_shoulder" parent:shoulderIKNode.parent
	shoulderFKNode.transform = shoulderIKNode.transform
	elbowFKNode = point size:5 cross:false box:true centermarker:false axistripod:false wirecolor:blue name:"help_fk_elbow" parent:shoulderFKNode
	elbowFKNode.transform = elbowIKNode.transform
	wristFKNode = point size:5 cross:false box:true centermarker:false axistripod:false wirecolor:blue name:"help_fk_wrist" parent:elbowFKNode
	wristFKNode.transform = WristIKNode.transform
	-- Create IK/FK switcher Node
	IKFKSwitcher = point size:3 cross:false box:true centermarker:false axistripod:false wirecolor:red name:"ctrl_ikfk_switcher" parent:shoulderIKNode.parent
	switcherMod = EmptyModifier()
	switcherMod.name = "IK FK switcher"
	addModifier IKFKSwitcher switcherMod
	CustAttributes.add IKFKSwitcher.modifiers[#IK_FK_switcher] IKFKSwitcherCA
	local IKFKController = bezier_float()
	IKFKSwitcher.modifiers[#IK_FK_SWITCHER].custAttributes[1].IKFKCtrl.controller = IKFKController
	IKFKSwitcher.modifiers[#IK_FK_SWITCHER].custAttributes[1].shoulderFKNode = shoulderFKNode
	IKFKSwitcher.modifiers[#IK_FK_SWITCHER].custAttributes[1].elbowFKNode = elbowFKNode
	IKFKSwitcher.modifiers[#IK_FK_SWITCHER].custAttributes[1].wristFKNode = wristFKNode
	IKFKSwitcher.modifiers[#IK_FK_SWITCHER].custAttributes[1].shoulderIKNode = shoulderIKNode
	IKFKSwitcher.modifiers[#IK_FK_SWITCHER].custAttributes[1].elbowIKNode = elbowIKNode
	IKFKSwitcher.modifiers[#IK_FK_SWITCHER].custAttributes[1].wristIKNode = wristIKNode
	IKFKSwitcher.modifiers[#IK_FK_SWITCHER].custAttributes[1].IKHandle = IKHandle
	IKFKSwitcher.modifiers[#IK_FK_SWITCHER].custAttributes[1].swivelIKNode = swivelIKNode
	-- Create constraints on the FK chain
	shoulderFKConstraint = Orientation_Constraint
	AddConstraint shoulderFKNode "rotation" shoulderFKConstraint "rotation list"
	shoulderFKNode.rotation.controller.appendTarget shoulderIKNode 50
	shoulderFKNode.rotation.controller = rotation_list()
	shoulderFKNode.rotation.controller.available.controller = Euler_XYZ()
	shoulderFKNode.rotation.controller.setName 1 "IK/FK"
	shoulderFKNode.rotation.controller.setName 2 "Animation"
	shoulderFKNode.rotation.controller.weights[1].controller = IKFKController
	shoulderFKNode.rotation.controller.active = 2
	elbowConstraint = Orientation_Constraint
	AddConstraint elbowFKNode "rotation" elbowConstraint "rotation list"
	elbowFKNode.rotation.controller.appendTarget elbowIKNode 50
	elbowFKNode.rotation.controller = rotation_list()
	elbowFKNode.rotation.controller.available.controller = Euler_XYZ()
	elbowFKNode.rotation.controller.setName 1 "IK/FK"
	elbowFKNode.rotation.controller.setName 2 "Animation"
	elbowFKNode.rotation.controller.weights[1].controller = IKFKController
	elbowFKNode.rotation.controller.active = 2
	wristConstraint = Orientation_Constraint
	AddConstraint wristFKNode "rotation" wristConstraint "rotation list"
	wristFKNode.rotation.controller.appendTarget wristIKNode 50
	wristFKNode.rotation.controller = rotation_list()
	wristFKNode.rotation.controller.available.controller = Euler_XYZ()
	wristFKNode.rotation.controller.setName 1 "IK/FK"
	wristFKNode.rotation.controller.setName 2 "Animation"
	wristFKNode.rotation.controller.weights[1].controller = IKFKController
	wristFKNode.rotation.controller.active = 2
)

makeIKFK $help_ik_handle $help_ik_elbow