/*
Copyright (C) 2019 Victor Chavanne <victor.chavanne@tutanota.com>

This file is part of cvAutoRig.

cvAutoRig is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

cvAutoRig is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with cvAutoRig.  If not, see <https://www.gnu.org/licenses/>.
*/

-- TODO :
	-- add auto name
	-- add freeze transform

function addBendybend = (
	-- Check if selection is valid
	selectionIsValid = True
	if selection.count != 2 then (
		selectionIsValid = False
		format "Wrong number of object selected. Must be 2, not %\n" selection.count
	)
	if (findString selection[1].name "ctrl" == undefined) or (findString selection[1].name "ctrl" == undefined) then (
		selectionIsValid = False
		format "Both selected controllers must have 'ctrl' in their name. This is not the case."
	)
	if (selection[1].parent != selection[2]) and (selection[2].parent != selection[1]) then (
		selectionIsValid = False
		format "One of the controllers must be the parent of the other. This is not the case."
	)
	
	-- Add the bendybend
	if selectionIsValid then (
		-- Define layer 1 nodes
		if (selection[1].parent == selection[2]) then (
			cL1_1 = selection[2]
			cL1_2 = selection[1]
		)
		else (
			cL1_1 = selection[1]
			cL1_2 = selection[2]
		)
		-- Create layer 2 nodes
		if (getNodeByName (substituteString cL1_1.name "_l1_" "_l2_")) != undefined then cL2_1 = (getNodeByName (substituteString cL1_1.name "_l1_" "_l2_")) -- Check for existing bendybend in the chain
		else (
			cL2_1 = circle radius:((amax cL1_1.width cL1_1.length) * 1.5) wirecolor:yellow
			cL2_1.transform = cL1_1.transform
			cL2_1.parent = cL1_1
			cL2_1.name = substituteString cL1_1.name "_l1_" "_l2_"
		)
		if (getNodeByName (substituteString cL1_2.name "_l1_" "_l2_")) != undefined then cL2_2 = (getNodeByName (substituteString cL1_2.name "_l1_" "_l2_")) -- Check for existing bendybend in the chain
		else (
			cL2_2 = circle radius:((amax cL1_2.width cL1_2.length) * 1.5) wirecolor:yellow
			cL2_2.transform = cL1_2.transform
			cL2_2.parent = cL1_2
			cL2_2.name = substituteString cL1_2.name "_l1_" "_l2_"
		)
		-- Create layer 3 nodes
		midPoint = point size:10 cross:true box:false axistripod:false centermarker:false wirecolor:green
		midPoint.transform.controller = transform_script()
		midPoint.transform.controller.AddNode "n1" cL2_1
		midPoint.transform.controller.AddNode "n2" cL2_2
		midPoint.transform.controller.script = "theMatrix = transmatrix ((n1.transform[4] + n2.transform[4]) / 2)\ntheMatrix[3] = normalize (n2.transform[4] - n1.transform[4])\ntheMatrix[2] = normalize (n1.transform[2])\ntheMatrix[1] = normalize (n1.transform[1])\ntheMatrix[2] = normalize (cross theMatrix[3] theMatrix[1])\ntheMatrix[1] = normalize (cross theMatrix[3] theMatrix[2])\ntheMatrix"
		midPoint.name = substituteString cL1_1.name "ctrl_l1" "help_bendyBend"
		cL3_1 = circle radius:((cL2_1.radius + cL2_2.radius) * 0.4) wirecolor:orange
		cL3_1.transform = midPoint.transform
		cL3_1.parent = midPoint
		cL3_1.name = substituteString cL1_1.name "_l1_" "_l3_"
		-- Create layer 4 nodes
		if (getNodeByName (substituteString cL1_1.name "_l1_" "_l4_")) == undefined then ( -- check if existing from previous bendybend
			cL4_1 = point size:5 cross:false box:true axistripod:false centermarker:false wirecolor:(color 255 204 153)
			cL4_1.transform = cL2_1.transform
			cL4_1.parent = cL2_1
			cL4_1.name = substituteString cL1_1.name "_l1_" "_l4_"
		)
		cL4_2 = point size:5 cross:false box:true axistripod:false centermarker:false wirecolor:(color 255 204 153)
		cL4_2.transform = cL3_1.transform
		cL4_2.parent = cL3_1
		cL4_2.name = substituteString cL1_1.name "_l1_" "_l4_"
		cL4_2.name = substituteString cL4_2.name "_l4_" "_l4_mid_"
		if (getNodeByName (substituteString cL1_2.name "_l1_" "_l4_")) == undefined then ( -- check if existing from previous bendybend
			cL4_3 = point size:5 cross:false box:true axistripod:false centermarker:false wirecolor:(color 255 204 153)
			cL4_3.transform = cL2_2.transform
			cL4_3.parent = cL2_2
			cL4_3.name = substituteString cL1_2.name "_l1_" "_l4_"
		)
	)
)

addBendybend()