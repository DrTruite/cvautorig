# Matrix :
Dans un transform controller, on decide de la position, de la rotation et du scale d'un seul coup. On y calcule la matrice de transformation.

Une matrice de tranformation est un tableau a 4 elements: 
```maxscript
theMatrix = transmatrix [0, 0, 0] -- creation d'un matrice de transformation
theMatrix[1] -- l'axe x
theMatrix[2] -- l'axe y
theMatrix[3] -- l'axe z
theMatrix[4] -- la position
```

# La position (theMatrix[4]) :
Pour créer une matrice positionné sur une node $TargetPos, on peut ecrire :
```maxscript
theMatrix = transmatrix targetPos.transform[4]
theMatrix -- ici on conclu la valeur du script comme etant egal a theMatrix.
```
On a simplement comme ca l'effet d'un position constraint. Si on veut la positionner entre plusieurs node ($Target1 $Target2 et $Target3 par exemple), on peut ecrire:
```maxscript
theMatrix = transmatrix ((target1.transform[4] + target2.transform[4] + target3.transform[4]) / 3 )
-- On fait ici la moyenne des position. On peut ecrire des calculs aussi compliqués qu'on veut
theMatrix
```

# La rotation :
C'est ici que ca se complique. On oublie tout ce qu'on sait sur Euler et autres quaternions. Ici on manipule directement les axes x, y et z du pivot de la node.

exemple de look at :
```maxscript
theMatrix = transmatrix targetPos.transform[4]
-- on créé un matrice qui restera positionné sur $targetPos

theMatrix[1] = normalize (lookAtTarget.transform[4] - parentNode.transform[4])
-- Un peu d'explication sur cette ligne :
-- theMatrix[1] represente l'axe X
-- En faisant une soustraction de deux positions, on obtient un vecteur : (lookAtTarget.transform[4] - parentNode.transform[4])
--     L'axe X va du coup s'orienter le long de ce vecteur
--     Le normalize va permettre d'obtenir un vecteur d'une taille de 1. Sans lui, la taille du vecteur X serait égal a la distance entre les deux nodes.

theMatrix
-- on conclut bien evidement toujours par cette ligne
```

Je t'invite a tester les trois lignes du dessus (en oubliant pas d'ajouter les bonne variables). Tu remarquera que le pivot en local est tout chelou. On a changé l'orientation de l'axe X, mais les autres ne bougent pas.

Pour ajouter un upNode, on peut du coup faire :
```maxscript
theMatrix = transmatrix targetPos.transform[4]
theMatrix[1] = normalize (lookAtTarget.transform[4] - parentNode.transform[4])

theMatrix[2] = normalize (upNode.transform[3])
-- Ici on utilise l'axe Y comme upNode. On l'aligne a l'axe Z d'un $upNode. On pourrais tres bien remplacer cette ligne par un deuxieme comportement de lookAt. On ajoute un normaize pour la forme, c'est probablemet inutile dans la majorité des cas.

theMatrix[3] = normalize (cross theMatrix[1] theMatrix[2])
-- Pour l'axe Z, on se contente de créer un vecteur perpendiculaire aux deux autres vecteurs

-- Comme les axes du pivots sont probablement toujours un peu bancals (je te laisse verifier), on ajoute la ligne suivante :
theMatrix[2] = normalize (cross theMatrix[1] theMatrix[3])
-- l'axe Y ne suivra pas exactement la direction désirée, mais en restera aussi proche que possible

theMatrix
```

# Le Scale :

Enfin, pour gerer le scale, on joue sur la longueur des vecteurs (d'ou l'interet des normalize).
Un axe X d'une longueur egale a 2 represente un scale de 2 en X. Ainsi, si par exemple on veut scaler l'object en X en fonction de sa distance avec son lookAt, on peut ecrire :
```maxscript
theMatrix = transmatrix targetPos.transform[4]
theMatrix[1] = normalize (lookAtTarget.transform[4] - parentNode.transform[4])
theMatrix[2] = normalize (upNode.transform[3])
theMatrix[3] = normalize (cross theMatrix[1] theMatrix[2])
theMatrix[2] = normalize (cross theMatrix[1] theMatrix[3])

theMatrix[1] *= distance targetPos lookAtTarget
-- Je sait pas si tu connais le *=
-- C'est equivalent a ecrire la ligne :
-- theMatrix[1] = theMatrix[1] * (distance targetPos lookAtTarget)

theMatrix
```
```

Voila. Tu peut créer tout plein de comportements super complexes et interessant de facon assez opti de cette facon. Enjoy ;)