# cvRig design document

## Layer tree

```
|--	default
|--	ch_characterName_variation
	|--	ch_characterName_variation_l1
	|--	ch_characterName_variation_l2
	|--	ch_characterName_variation_l3
	|--	ch_characterName_variation_l4
	|-- ch_characterName_variation_rig
	|-- ch_characterName_variation_mdl
```

## Controller layers
There are four layers of controllers.

Layer1 is biped logic. Most of the character is animated here.
IK/FK is here

Layer2 is for silhouette deformation. Clothes and accessories controllers are here.

Layer3 is for additionnal silhouette deformation.

Layer4 is where the skinning bones are. They are provided as controllers just in case. Should not be used outside of exceptionnal occasion

## Nomenclature
Everything is small caps. Always.
Node names as as follow :

```
ch	_characterName	_characterVariation	_ctrl	_lx		_[r/l/m]	_[front/back/top/bot]	_bodyPartName	_[...]	_01
	
[1]	_[2]			_[3]				_[4]	_[5]	_[6]		_[7]					_[8]			_[9]	_[10]
```

- [1] ch for character
- [2] name of the character (ex: bob)
- [3] additional info when the character have more than one version (ex: casualclothing)
- [4] ctrl for controllers, mdl for geometry, help for rigging elements
- [5] layer number for controller nodes. Can be l1, l2, l3 or l4
- [6] right/left/middle information when necessary. can be r, l or m
- [7] front/back/top/bottom information when necessary. can be front, back, top or bot
- [8] name of the body part or accessory
- [9] any number of additionnal info on the part
- [10] number for when there are several similar items